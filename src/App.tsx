import React, { Suspense } from 'react';
import { Router, Route, Routes } from 'react-router-dom';
import ProjectList from './components/projectList';
import UserForm from './components/userForm';
import UsersList from './components/usersList';
import Dashboard from './pages/dashboard';

const Login = React.lazy(() => import('./pages/login'))

const App = () => {
    return (
        <>
            <Suspense fallback={<></>}>
                <Routes>
                    <Route path="/login" element={<Login />} />
                    <Route path="/" element={<Dashboard />}>
                        <Route path="/" element={<UsersList />} />
                        <Route path="/projects" element={<ProjectList />} />

                        <Route path="/add/user" element={<UserForm />} />
                        <Route path="/users" element={<UsersList />} />
                        {/* <Route path="/modal" element={<ModalComponent callback={()=>{}}/>} /> */}
                    </Route>
                    <Route path="*" element={<Dashboard />} />

                </Routes>
            </Suspense>
        </>)
}

export default App;