import React from "react";
import { useAuth } from "../../context/AuthProvider/useAuth"
import Login from '../../pages/login';

export const ProtectedLayout = ({ children }: { children: JSX.Element }) => {
    const auth = useAuth();
    
    if (!auth.email) {
        return <Login/>
    }
    return children;
}