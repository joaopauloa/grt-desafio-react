import React from "react";
import './style.css';
import { Modal } from 'antd';
import { useAuth } from "../../context/AuthProvider/useAuth";
import { deleteUser } from "../../services/userService";



interface Props {
    userID: number;
    callback(): void;
}

function AlertModal(props: Props) {
    const user = useAuth();

    const persistence = async () => {
        await deleteUser(user, props.userID);
        props.callback();
    }

    return (
        <div>
            <Modal
                title="Alert"
                visible={true}
                onOk={() => {persistence()}}
                onCancel={() => { props.callback() }}
            >
                <span className="input-label">Are you sure you want to delete this user</span>

            </Modal>
        </div>
    );
}

export default AlertModal;