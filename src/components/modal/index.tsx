import React from "react";
import { useState } from "react";
import './style.css';
// import { AiOutlineClose } from "react-icons/ai"
import { Modal, Button } from 'antd';
import { useAuth } from "../../context/AuthProvider/useAuth";
import { useNavigate } from "react-router-dom";
import { updateUser } from "../../services/userService";
import { message } from "antd";

interface ModalProps {
  item: any;
  callback(): void;

}

function ModalComponent(props: ModalProps) {
  const user = useAuth();
  const navigate = useNavigate();

  const [name, setName] = useState(props.item?.userName);
  const [cpf, setCpf] = useState('');
  const [rg, setRG] = useState(props.item?.rg);
  const [birthDate, setBirthDate] = useState(props.item?.birthdate);
  const [motherName, setMotherName] = useState(props.item?.motherName);
  const [email, setEmail] = useState(props.item?.userEmail);

  const validateFields = () => {
    const reEmail = /\S+@\S+\.\S+/;
    const reCPF = /^\d+$/;
    const reRG = /^\d+$/;
    const reDate = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
    const result = reCPF.test(cpf) && reEmail.test(email) && reRG.test(rg) && reDate.test(birthDate) && motherName !== null && name !== null && cpf.length === 11 && (motherName !== undefined && name !== undefined);
    return result;
  };

  const persistence = async () => {
    const editUser: any = {
      'userId':props.item?.userId,
      'birthdate': birthDate,
      'cpf': cpf,
      'motherName': motherName,
      'rg': rg,
      'userEmail': email,
      'userName': name,
    }

    const result = await updateUser(user, editUser);
    navigate('/users');
    
    if (result === null) {
      message.error('error creating user');
    }
  }

  return (
    <div>
      <Modal
        title="Edit User"
        visible={true}
        onOk={() => { if(validateFields()){
          persistence();
          props.callback();            
        }else{
          message.error('There is some empty field');
        }
      }}
        onCancel={props.callback}
      >
        <span className="input-label">User name</span><br />
        <input className="input-text-modal" placeholder="userName"
          value={name}
          required={true}
          onChange={event => { setName(event.target.value) }} /><br />

        <span className="input-label">CPF</span><br />
        <input className="input-text-modal" placeholder="apenas número"
          value={cpf}
          required={true}
          onChange={event => { setCpf(event.target.value) }} /><br />

        <span className="input-label">RG</span><br />
        <input className="input-text-modal" placeholder="apenas número"
          value={rg}
          required={true}
          onChange={event => { setRG(event.target.value) }} /><br />

        <span className="input-label">Birth date</span><br />
        <input className="input-text-modal" placeholder="2022-09-09"
          value={birthDate}
          required={true}
          onChange={event => { setBirthDate(event.target.value) }} /><br />

        <span className="input-label">Mother name</span><br />
        <input className="input-text-modal" placeholder="Mother name"
          value={motherName}
          required={true}
          onChange={event => { setMotherName(event.target.value) }} /><br />

        <span className="input-label">Email</span><br />
        <input className="input-text-modal" placeholder="joao@gmail.com"
          value={email}
          required={true}
          onChange={event => { setEmail(event.target.value) }} /><br />

      </Modal>
    </div>
  );
}

export default ModalComponent;