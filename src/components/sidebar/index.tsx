import React, { useState } from 'react';
import { useAuth } from '../../context/AuthProvider/useAuth';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { BsCheckCircle } from 'react-icons/bs';
import { Link } from 'react-router-dom';
import { SidebarData } from './sidebarData';
import './style.css';
import { IconContext } from 'react-icons';


function Navbar() {
  const [sidebar, setSidebar] = useState(false);
  const auth = useAuth();

  const showSidebar = () => setSidebar(!sidebar);

  return (
    <>
      <IconContext.Provider value={{ color: '#fff' }}>
        <div className='navbar'>
          <Link to='#' className='menu-bars'>
            <FaIcons.FaBars size={26} onClick={showSidebar} />
          </Link>
          <BsCheckCircle size={30} color='yellow' style={{ marginLeft: 10 }} />
          <h3 className="title">GREat</h3>
        </div>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
          <ul className='nav-menu-items' onClick={showSidebar}>
            <li className='navbar-toggle'>
              <Link to='#' className='menu-bars-close-icon' >
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
            {SidebarData.map((item, index) => {
              return (
                <li key={index} className={item.cName}>
                  <Link to={item.path}>
                    {item.icon}
                    <span>{item.title}</span>
                  </Link>
                </li>
              );

            })}
            <li className="nav-text">
              <Link to={'#'} onClick={() => {
                auth.logout();
              }}>
                <AiIcons.AiOutlinePoweroff />
                <span>Logout</span>
              </Link>
            </li>
          </ul>
        </nav>
      </IconContext.Provider>
    </>
  );
}

export default Navbar;