import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';

export const SidebarData = [

  {
    title: 'Users',
    path: 'users',
    icon: <IoIcons.IoMdPeople />,
    cName: 'nav-text'
  },

  {
    title: 'Create user',
    path: 'add/user',
    icon: <AiIcons.AiOutlineUserAdd />,
    cName: 'nav-text'
  },


  // {
  //   title: 'Logout',
  //   path: 'logout',
  //   icon: <AiIcons.AiOutlinePoweroff />,
  //   cName: 'nav-text'
  // }
];