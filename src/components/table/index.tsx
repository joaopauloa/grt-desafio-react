import React from "react";
import { useNavigate } from 'react-router-dom';
import { useState } from "react";
import { useAuth } from "../../context/AuthProvider/useAuth";
import { FcEmptyFilter } from "react-icons/fc";
import { FaUserEdit } from "react-icons/fa";
import { MdDelete } from "react-icons/md";


import ModalComponent from "../modal";
import AlertModal from "../alertModal";

interface ITable {
    itens: any[],
    editFunction(): void;
    deleteFunction(): void;
    pageSize: number

}

const TableComponent = (props: ITable) => {
    const user = useAuth();
    const navigate = useNavigate();
    const [showEditUserModal, setShowEditUser] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [item, setItem] = useState(null);
    const [userId, setUserID] = useState(-1);


    const edit = (item: any) => {
        setItem(item)
    }

    const modalManager = () => {
        setShowEditUser(false);
        window.location.reload();

    }

    const closeModal = () => {
        setShowModal(false);
        window.location.reload();

    }

    const deleteUser = (id:number)=>{
        setUserID(id);
        setShowModal(true);
    }

    return (<>
        {showEditUserModal ? <ModalComponent callback={modalManager} item={item} /> : null}
        {showModal ? <AlertModal callback={closeModal} userID={userId} /> : null}
        <table className="customers">
            <tbody>
                <tr>
                    <th>Name</th>
                    <th>Mother Name</th>
                    <th>Birth Date</th>
                    <th>E-mail</th>
                    <th>CPF</th>
                    <th>RG</th>
                    <th>Creation date</th>
                    <th>Actions</th>
                </tr>
                {
                    props.itens?.length > 0 ?
                        props.itens.map((item, index) => (

                            <tr key={index}  >
                                <td className="tableColName" >{item?.projectName || item?.userName}</td>
                                <td className="tableColName" >{item?.projectName || item?.motherName}</td>
                                <td className="tableColName" >{item?.projectName || item?.birthdate}</td>
                                <td className="tableColName" >{item?.projectName || item?.userEmail}</td>
                                <td className="tableColName" >{item?.projectName || item?.cpf}</td>
                                <td className="tableColName" >{item?.projectName || item?.rg}</td>
                                <td className="tableColName" >{item?.projectName || item?.creationDate}</td>
                                <td style={{ textAlign: "center" }}>
                                    <a className="table-action" onClick={() => {
                                        edit(item);
                                        setShowEditUser(true);

                                    }} ><FaUserEdit size={25} /></a>
                                    {item.permission === null ? <a className="table-action"
                                        onClick={() => { deleteUser(item.userId) }}
                                    ><MdDelete size={25} style={{ margin: 3, color: 'red' }} /></a> : null}
                                </td>
                            </tr>
                        ))
                        :
                        <tr >
                            <td className="emptyTable" colSpan={8} style={{ height: 100, }}><FcEmptyFilter size={60} /></td>
                        </tr>
                }
            </tbody>
        </table>
    </>
    );
};

export default TableComponent; 
