import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";
// import ModalComponent from "../modal";
import { addUser } from "../../services/userService";
import { useAuth } from "../../context/AuthProvider/useAuth";
import { UserDTO } from '../../context/AuthProvider/types';
import { message } from "antd";
import './style.css';

const UserForm = () => {
    const user = useAuth();
    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [cpf, setCpf] = useState('');
    const [rg, setRG] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [motherName, setMotherName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // const [showMessage, setShowMessage] = useState(false);

    const validateFields = () => {
        const reEmail = /\S+@\S+\.\S+/;
        const reCPF = /^\d+$/;
        const reRG = /^\d+$/;
        const reDate = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
        return reEmail.test(email) && reCPF.test(cpf)&& reRG.test(rg) && reDate.test(birthDate) && motherName!=='', name !== '' && password !== '' && cpf.length === 11;
    };

    async function persistence() {
        const newUser: UserDTO = {
            birthdate: birthDate,
            cpf: cpf,
            motherName: motherName,
            rg: rg,
            userEmail: email,
            userName: name,
            password: password
        }

        const response = await addUser(user, newUser);
        navigate('/users');

        if (response === null) {
            message.error('error creating user');
        }else{
            message.success("success!", 8);
            window.location.reload();
        }
    }

    return (
        <>
            <div className="container">
                {/* {showMessage ? <ModalComponent callback={() => {
                    setShowMessage(false);
                    console.log('click');
                }} /> : null} */}

                <div className="content">
                    <h1>Create User</h1>
                    <div>
                        <span className="input-label">User name</span><br />
                        <input className="input-text" placeholder="userName"
                            required={true}
                            onChange={event => setName(event.target.value)} /><br />

                        <span className="input-label">CPF</span><br />
                        <input className="input-text" placeholder="apenas número"
                            required={true}
                            onChange={event => setCpf(event.target.value)} /><br />

                        <span className="input-label">RG</span><br />
                        <input className="input-text" placeholder="apenas número"
                            required={true}
                            onChange={event => setRG(event.target.value)} /><br />


                        <span className="input-label">Birth date</span><br />
                        <input className="input-text" placeholder="2022-09-09"
                            required={true}
                            onChange={event => setBirthDate(event.target.value)} /><br />

                        <span className="input-label">Mother name</span><br />
                        <input className="input-text" placeholder="Mother name"
                            required={true}
                     
                            onChange={event => setMotherName(event.target.value)} /><br />

                        <span className="input-label" >User email</span><br />
                        <input className="input-text"
                            type="email"
                            placeholder="exemplo@great.ufc.br"
                            onChange={event => setEmail(event.target.value)} /><br />

                        <span className="input-label">Password</span><br />
                        <input className="input-text"
                            type="password"
                            onChange={event => setPassword(event.target.value)} /><br />

                        <Link to="/users">
                            <button className="cancel-button">Cancel</button>
                        </Link>
                        <a className="confirmation-button" type="submit" onClick={(event) => {
                            if (validateFields()) {
                                persistence();
                            } else {
                                message.error('There is some empty field');
                            }
                        }} >Create</a>

                    </div>
                </div>
            </div>
        </>);
};
export default UserForm;