import React, { useState, useEffect } from "react";
import { getAllUsers, searchAPI } from '../../services/userService';
import { useAuth } from "../../context/AuthProvider/useAuth";
import { Pagination } from 'antd';
import { Input } from 'antd';
import { Select } from 'antd';

import './style.css';
import TableComponent from "../table";
import Item from "antd/lib/list/Item";


const UsersList = () => {
    const { Search } = Input;
    const { Option } = Select;
    const user = useAuth();
    const [users, setUsers] = useState([]);
    const [pageNumber, setPageNumber] = useState(0);
    const [size, setPageSize] = useState(10);
    const [searcString, setSearchString] = useState('');
    const [searchOption, setSearchOption] = useState('username');

    const search = async () => {
        if (searcString !== null && searcString !== '') {
            const result = await searchAPI(user, searchOption, searcString);
            setUsers(result.content);
        } else {
            searchUser();
        }
    }

    async function searchUser() {
        const listContent = await getAllUsers(user, pageNumber, size);
        setUsers(listContent.content);
        setPageSize(listContent.size);
    }

    const changePageNumber = (newPageNumber:number)=>{
        setPageNumber(newPageNumber);
        searchUser();

    }

    useEffect(() => {
        searchUser();
    }, []);

    return (
        <>
            <div className="container">
                <div className="content">
                    <h1>Users</h1>
                    <div style={{ width: 400, display: 'flex', marginLeft: 380}}>
                        <span className="input-label"></span><br />
                        <Select defaultValue="username" style={{ width: 200 }} onChange={(e) => { setSearchOption(e)}}>
                            <Option value="username">Name</Option>
                            <Option value="cpf">CPF</Option>
                            <Option value="rg">RG</Option>
                        </Select>

                        <Search placeholder="input search text" size="middle" onChange={(e) => { setSearchString(e.target.value) }}
                            enterButton onSearch={() => { search() }} />
                    </div>
                    <TableComponent itens={users} editFunction={() => { }} deleteFunction={() => { }} pageSize={size} />
                    {users.length > 0 ? <div style={{ width: 400, display: 'flex', marginLeft: 450 }}> <Pagination defaultCurrent={1} total={size} onChange={(e) => { changePageNumber(e-1)  }} />
                    <span className="input-label"></span><br />
                        <Select defaultValue="10" style={{ width: 100 }} onChange={(e) => { setPageSize(parseInt(e)-1)}}>
                            <Option value="10">10</Option>
                            <Option value="15">15</Option>
                            <Option value="20">20</Option>
                        </Select>
                    </div>


                        : null}
                </div>
            </div>
        </>);
};


export default UsersList;