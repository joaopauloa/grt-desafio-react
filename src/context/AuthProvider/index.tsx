import React, { createContext, useEffect, useState } from "react";
import { IAuthProvider, IContext, IUser } from "./types";
import { getUserLocalStorage, LoginRequest, setUserLocalStorage, Logout } from "./utils";


export const AuthContext = createContext<IContext>({} as IContext)

export const AuthProvider = ({ children }: IAuthProvider) => {
    const [user, setUser] = useState<IUser | null>();

    useEffect(() => {
        const user = getUserLocalStorage();
        if (user) {
            setUser(user);
        }
    }, [])

    async function authenticate(email: string, password: string) {
        const response = await LoginRequest(email, password);
        const payload = { userId: response.userId, userName: response.userName, token: response.token, email }
        setUser(payload);
        setUserLocalStorage(payload);
    }
    

    function logout() {
        setUser(null);
        Logout();
    }

    return (
        <AuthContext.Provider value={{ ...user, authenticate, logout }}>
            {children}
        </AuthContext.Provider>
    )
}