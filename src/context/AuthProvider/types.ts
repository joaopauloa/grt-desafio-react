export interface IUser {
    userId?: string,
    userName?: string,
    email?: string,
    token?: string
}

export interface UserDTO {
    birthdate: string,
    cpf: string,
    motherName: string,
    rg: string,
    userEmail: string,
    userName: string,
    password:string
}

export interface IContext extends IUser {
    authenticate: (email: string, password: string) => Promise<void>;
    logout: () => void;
}

export interface IAuthProvider {
    children: JSX.Element;
}