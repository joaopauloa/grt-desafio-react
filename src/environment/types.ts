export interface Project {
    projectId: number,
    projectName: string,
    actions: string
}

export interface IProject{
    id: number,
    name: string
};

export interface UseCase{
    id: number;
    title: string
};