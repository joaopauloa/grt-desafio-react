import React from 'react';
import {Outlet} from 'react-router-dom';

import './style.css';
import Sidebar from '../../components/sidebar';

export default class Dashboard extends React.Component {
  render() {
    return (
      <>
        <Sidebar/>
        <Outlet/>
      </>
    );
  }
}