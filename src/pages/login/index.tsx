import React, { useState } from 'react';
import { BsCheckCircle } from 'react-icons/bs';
import { useAuth } from '../../context/AuthProvider/useAuth';
import { useNavigate } from 'react-router-dom';
import {message} from 'antd';

import './style.css';
// import Sidebar from '../../components/sidebar';


const Login = () => {
  
  const auth = useAuth();
  const navigate = useNavigate();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  
  async function onFinish(email: string, password: string) {
    try {
      
      await auth.authenticate(email, password);
      navigate('/users')

    } catch (error) {
      message.error('Incorrect email or password', 3);
    }
  }


  return (
    <>
      <div className='login_main'>
        <p className="sign"><BsCheckCircle size={30} style={{ marginLeft: 10 }} />GREat</p>
        <form className="form1">

          <input className="un" type="text" placeholder="Username" 
          onChange={(event)=>setEmail(event.target.value)} />

          <input className="pass" type="password" placeholder="Password"
           onChange={(event)=>setPassword(event.target.value)}/ >

          <a className="submit" 
          onClick={()=>{
            onFinish(email, password);
          }}
          >Sign in</a>
        </form>
      </div>

    </>
  );
}

export default Login;