import { IContext, IUser } from '../context/AuthProvider/types';
import Api from './api';

export async function getAllProjects(user: IUser) {
    const tokenStr = `Bearer ${user.token}`;
    try {
        const request = await Api.get("/api/project/request/all", { headers: { "Authorization": tokenStr } });
        return request.data;
    } catch (err) {
        return null;
    }
};

export async function createProject(user: IContext, projectName: string, projectDescription: string) {
    const tokenStr = `Bearer ${user.token}`;
    const request = await Api.post("/api/project/", {
        project: {
            "projectName": projectName
        },
        user: {
            "userName": user.userName
        }
    }, { headers: { "Authorization": tokenStr } }).then((result) => {
        return result;
    }).catch((err) => {
        if (err?.response?.status === 401) {
            user.logout();
            return null;
        }
    });

    return request;

};