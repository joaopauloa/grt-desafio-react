import { IContext, UserDTO } from '../context/AuthProvider/types';
import Api from './api';


export async function addUser(contextUser: IContext, newUser: UserDTO) {
    const tokenStr = `Bearer ${contextUser.token}`;
    const request = await Api.post(`/api/user`, newUser, { headers: { "Authorization": tokenStr } }).then((result) => {
        return result.data;
    }).catch((err) => {
        if (err.response.status === 401) {
            contextUser.logout();
        }
        return null;
    });
    return request;
};


export async function updateUser(contextUser: IContext, newUser: UserDTO) {
    const tokenStr = `Bearer ${contextUser.token}`;
    const request = await Api.put(`/api/user`, newUser, { headers: { "Authorization": tokenStr } }).then((result) => {
        return result.data;
    }).catch((err) => {
        if (err?.response?.status === 401) {
            contextUser.logout();
        }
        return null;
    });
    return request;
};

export async function getUserByName(user: IContext, userName: string) {
    const tokenStr = `Bearer ${user.token}`;
    try {
        const request = await Api.get(`/api/user/request/${userName}`, { headers: { "Authorization": tokenStr } });
        if (request.status === 401) {
            user.logout();
        }
        return request.data;
    } catch (err) {
        console.log(err);
        return null;
    }
};

export async function searchAPI(user: IContext, url:string,  parameter: string) {
    const tokenStr = `Bearer ${user.token}`;
    try {
        const request = await Api.get(`/api/user/${url}/${parameter}`, { headers: { "Authorization": tokenStr } });
        if (request.status === 401) {
            user.logout();
        }
        return request.data;
    } catch (err) {
        console.log(err);
        return null;
    }
};


export async function getAllUsers(user: IContext, page:number, size:number) {
    const tokenStr = `Bearer ${user.token}`;
    const request = await Api.get(`/api/user/${page}/${size}`, { headers: { "Authorization": tokenStr } }).then((result) => {
        return result.data;
    }).catch((err) => {
        if (err?.response?.status === 401) {
            user.logout();
        }
        return err;
    });
    return request;
};


export async function deleteUser(contextUser: IContext, userId:number) {
    const tokenStr = `Bearer ${contextUser.token}`;
    const request = await Api.delete(`/api/user/${userId}`, { headers: { "Authorization": tokenStr } }).then((result) => {
        return result.data;
    }).catch((err) => {
        alert(err.message)
        return null;
    });
    return request;
};