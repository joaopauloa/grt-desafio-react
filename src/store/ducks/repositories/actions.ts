import {action} from 'typesafe-actions';
import { RespositoriesTypes, Repository } from './types';

export const loadRequests = () => action(RespositoriesTypes.LOAD_REQUESTS)

export const loadSuccess = (data: Repository[]) => action(RespositoriesTypes.LOAD_SUCCESS, {data})

export const loadFailure = () => action(RespositoriesTypes.LOAD_FAILURE)