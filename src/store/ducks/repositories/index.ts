import { Reducer } from 'redux';
import { RepositoryState, RespositoriesTypes } from "./types"

const INITIAL_STATE: RepositoryState = {
    data: [],
    error: false,
    loading: false
}

const reducer: Reducer<RepositoryState> = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case RespositoriesTypes.LOAD_REQUESTS:
            console.log('LOAD_REQUESTS')
            return { ...state, loading: true }
        case RespositoriesTypes.LOAD_SUCCESS:
            console.log('LOAD_SUCCESS')
            return { ...state, loading: false, error: false, data: action.payload.data };
        case RespositoriesTypes.LOAD_FAILURE:
            console.log('LOAD_FAILURE')
            return { ...state, loading: false, error: true, data: [] }
        default:
            console.log('DEFAULT')
            return state
    }
}

export default reducer;