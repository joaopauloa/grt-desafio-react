import { call, put } from "redux-saga/effects";
import Api from "../../../services/api";
import {getUserLocalStorage} from "../../../context/AuthProvider/utils";
import { loadSuccess, loadFailure } from "./actions";

export function* load():any{
    try{
        const user = getUserLocalStorage();
        console.log('USER');
        // let response = yield call(Api.get, '/users/joaopauloa/repos', { headers: { "Authorization": tokenStr })
        yield put(loadSuccess([{id:0, name:'joao'}]))
    } catch(err){
        yield put(loadFailure())

    }
}