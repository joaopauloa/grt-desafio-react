export enum RespositoriesTypes{
    LOAD_REQUESTS = '@reposotories/LOAD_REQUEST',
    LOAD_SUCCESS =  '@reposotories/LOAD_SUCCESS',
    LOAD_FAILURE =  '@reposotories/LOAD_FAILURE'
}
// Data Type
export interface Repository{
    id: number
    name: string
}

//State type
export interface RepositoryState{
   readonly data: Repository[]
   readonly loading: boolean
   readonly error: boolean
}