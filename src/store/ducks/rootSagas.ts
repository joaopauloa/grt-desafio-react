import { all, takeLatest } from "redux-saga/effects";

import {RespositoriesTypes} from './repositories/types';
import { load } from "./repositories/sagas";

export default function* rootSaga():any{
    console.log('rootSagas')
    return yield all([
        takeLatest(RespositoriesTypes.LOAD_REQUESTS, load),
    ])
}