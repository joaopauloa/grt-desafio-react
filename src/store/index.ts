import { createStore } from 'redux';
// import { IProject, UseCase } from '../environment/types';

const INITIAL_STATE: any = {
    project: { id: 0, name: 'Default' }
}

function reducer(state: any = INITIAL_STATE, action: any) {
    if (action.type === 'SELECT_PROJECT') {
        console.log(action);
        return { ...state, project: action.project }
    }else{
        return state;
    }
}


const store = createStore(reducer);
export default store;
