import { createStore, applyMiddleware, Store } from "redux";
import createSagaMiddleware from "redux-saga";
import { RepositoryState } from "./ducks/repositories/types";

import rootReducer from "./ducks/rootReducer";
import rootSaga from "./ducks/rootSagas";

export interface ApplicationState {
    repositories: RepositoryState
}

const sagaMiddleware = createSagaMiddleware();

const store: Store<ApplicationState> = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga)

export default store;